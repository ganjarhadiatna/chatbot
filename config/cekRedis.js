require('dotenv').config()

const redis = require('redis')
const redisUrl = process.env.REDIS_HOST
const redisPort = process.env.REDIST_PORT
const redisPassword = process.env.REDIS_PASSWORD
const client = redis.createClient(redisUrl, redisPort, redisPassword)

module.exports = {
    getCached: (req, res, next) => {
        const { redis_key } = req.headers
        client.get(redis_key, function (err, reply) {
            if (err) {
                res.status(500).json({
                    message: "Something wen wrong"
                })
            }

            if (reply == null) {
                next()
            } else {
                res.status(200).json({
                    message: `Success read ${redis_key}`,
                    data: JSON.parse(reply)
                })
            }
        })
    },
    caching: (key, data) => {
        client.set(key, JSON.stringify(data))
    },
    delCaching: (key) => {
        client.del(key)
    }
}