'use strict'

require('dotenv').config()

const line = require('@line/bot-sdk')
const restify = require('restify')
const mongoose = require('mongoose')
const handle = require('./handles/index')
// const cekRedis = require('./config/cekRedis')

// redis
// const redis = require('redis')
// const redisUrl = process.env.REDIS_HOST
// const client = redis.createClient(redisUrl)

// cekRedis

// create line SDK config variables
const config = {
    channelAccessToken: process.env.CHANNEL_ACCESS_TOKEN,
    channelSecret: process.env.CHANNEL_SECRET,
}

// create express app
const app = restify.createServer()

// register a webhook handler with middleware
app.post(
    '/webhook', 
    line.middleware(config), 
    (req, res) => {
        Promise
            .all(req.body.events.map((event) => {
                handle(event, req)
            }))
            .then((result) => res.json(result))
            .catch((err) => {
                console.log(err)
            })
    }
)

app.get('/', (req, res) => {
    res.send('Hello World!')
})

// listen on port
const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log('[+] No db');
    console.log(`[+] Server listening on ${app.url}/webhook PORT ${port}`)
})

// mongoose
// if (process.env.MONGODB_URI) {
//     console.log('[+] With db')
    
//     app.listen(port, () => {
//         mongoose.set('useFindAndModify', false)
//         mongoose.connect(process.env.MONGODB_URI, {
//             useNewUrlParser: true
//         })
//     })

//     const db = mongoose.connection
//     db.on('error', (err) => console.log(err))
//     db.once('open', () => {
//         console.log(`[+] Server with DB Listening on PORT ${app.url}/webhook PORT ${port}`)
//     })

// } else {

//     app.listen(port, () => {
//         console.log('[+] No db');
//         console.log(`[+] Server listening on ${app.url}/webhook PORT ${port}`)
//     })
    
// }